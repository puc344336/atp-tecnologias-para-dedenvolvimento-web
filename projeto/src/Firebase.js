import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyANAHXEhl3YRrGVEr82XHjCSJPsguSm-5k",
    authDomain: "atpweb-e66ff.firebaseapp.com",
    projectId: "atpweb-e66ff",
    storageBucket: "atpweb-e66ff.appspot.com",
    messagingSenderId: "1009427457854",
    appId: "1:1009427457854:web:ca046048b469099dc66e6f"
  };

  if(!firebase.apps.length){
    firebase.initializeApp(firebaseConfig)

  }

  export default firebase;