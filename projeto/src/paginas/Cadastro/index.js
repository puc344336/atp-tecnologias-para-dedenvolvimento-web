import React, { Component } from "react";
import { Link } from 'react-router-dom'
import firebase from '../Firebase'

class Cadastro extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            sobrenome: "",
            email:"",
            senha:"",
            dados:[]

        }

        this.gravar = this.gravar.bind(this);
        this.listar = this.listar.bind(this);
        this.criarUsuario = this.criarUsuario.bind(this);


    }

    async gravar() {

        await firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
        .then( async (retorno)=>{

            await firebase.firestore().collection("usuario").doc(retorno.user.uid).set({
                nome: this.state.nome,
                sobrenome: this.state.sobrenome
            })
        });


    }

        listar ()
        {firebase.firestore().collection("usuario").get().then((retorno) => {
            var state = this.state;
            retorno.forEach((item) => {
                this.state.dados.push({
                    id: item.id,
                    nome: item.data().nome,
                    sobrenome: item.data().sobrenome
                });
            });
            this.setState(state);
        });
    }

    criarUsuario(){
        firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.senha)
    }

        render() {
            return (
                <div>
                    <h1>Página de Cadastro</h1>

                    <input type="text" placeholder='email' onChange={(e) => this.setState({ email: e.target.value })} />                    <br />

                    <input type="password" placeholder='senha' onChange={(e) => this.setState({ senha: e.target.value })} />
                    <br />
                    <input type="text" placeholder='Nome' onChange={(e) => this.setState({ nome: e.target.value })} />
                    <br />
                    <input type="text" placeholder='Sobrenome' onChange={(e) => this.setState({ sobrenome: e.target.value })} />
                    <br />             


                    <br />
                    <button onClick={this.gravar}>Gravar</button>

                    <button onClick={this.listar}>Listar</button> <br/>

                

<ul>
    {this.state.dados.map((item)=>{
      return(
        <li> {item.nome + " " + item.sobrenome} </li> 
      )
    })}
</ul>


                </div>
            )
        }

    };

export default Cadastro


