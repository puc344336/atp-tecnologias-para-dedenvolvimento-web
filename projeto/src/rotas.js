import {BrowserRouter, Route, Routes, Switch} from 'react-router-dom';

import Home from './paginas/Home';
import Sobre from './paginas/Sobre';
import Contato from './paginas/Contato';
import NotFound from './paginas/NotFound';
import Cadastro from './Login';
import Principal from './paginas/Principal';


const Rotas = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route exact={true} path="/home" element={<Home/>} />
                <Route exact={true} path="/sobre" element={<Sobre/>} />
                <Route exact={true} path="/contato" element={<Contato/>} />
                <Route exact={true} path="*" element={<NotFound/>} />
                <Route exact={true} path="/" element={<Cadastro/>} />
                <Route exact={true} path="/principal" element={<Principal/>} />


            </Routes>
        </BrowserRouter>
    )
}

export default Rotas;

